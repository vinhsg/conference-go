import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    query = {"query": f"{city}, {state}"}
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url, headers=headers, params=query)
    content = json.loads(response.content)
    # picture_url = content["photos"][19000]["src"]["original"]
    print(content)
    # print(picture_url)


get_photo("New York", "New York")

#     r = requests.get(urls, headers=headers)


def get_weather_data(city, state):
    url = "https://api.openweathermap.org/geo/1.0/direct"
    query = {"q": f"{city}, {state}, US"}
    headers = {"appid": OPEN_WEATHER_API_KEY}
    response = requests.get(url, query, headers)
    content = json.loads(response.content)
    print(content)

    # {city name}, {state code}, {country code}&limit =  {limit}&appid= {API key}
